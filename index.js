/* 
    Quiz:
    1. How do you create arrays in JS?
    answer:
    create a variable for the array name and enclosed the data into a square bracet

    2. How do you access the first character of an array?
       Answer: arrayName[0]

    3. How do you access the last character of an array?
     answer: 
     let students = ["John", "Joe", "Jane", "Jessie"];
        const lastStudents = students.splice(-1)
        console.log(lastStudents)

    4. What array method searches for, and returns the index of a given value in an array? This method returns -1 if given value is not found in the array.
    answer: indexOf()

    5. What array method loops over all elements of an array, performing a user-defined function on each iteration?
    Answer: forEach()
    
    6. What array method creates a new array with elements obtained from a user-defined function?
    Answer: map()

    7. What array method checks if all its elements satisfy a given condition?
    Answer: every()

    8. What array method checks if at least one of its elements satisfies a given condition?
    Answer: some()

    9. True or False: array.splice() modifies a copy of the array, leaving the original unchanged.
    Answer: False

    10.True or False: array.slice() copies elements from original array and returns them as a new array.
    Answer: True

*/


// Function Coding:

let students = ["John", "Joe", "Jane", "Jessie"];

/* 1. Create a function named addToEnd that will add a passed in string to
 the end of a passed in array.If element to be added is not a string,
 return the string "error - can only add strings to an array".Otherwise,
 return the updated array.Use the students array and the string "Ryan"
 as arguments when testing */

const addToEnd = (array, newStudents) => {

    if (typeof newStudents === "string") {
        array.push(newStudents);
        return array;
    }

    else {
        return "error - can only add strings to an array"
    }
}
console.log(addToEnd(students, "Ian"));
console.log(addToEnd(students, 123));


/* 2. Create a function named addToStart that will add a passed in string to
the start of a passed in array. If element to be added is not a string, return
the string "error - can only add strings to an array". Otherwise, return the
updated array. Use the students array and the string "Tess" as arguments
when testing. */

const addToStart = (array, newStudents) => {

    if (typeof newStudents === "string") {
        array.unshift(newStudents);
        return array;
    }

    else {
        return "error - can only add strings to an array"
    }
}
console.log(addToStart(students, "Christoper"));
console.log(addToStart(students, 123));



/* 3. Create a function named elementChecker that will check a passed in
array if at least one of its elements has the same value as a passed in
argument. If array is empty, return the message "error - passed in array is
empty". Otherwise, return a boolean value depending on the result of the
check. Use the students array and the string "Jane" as arguments when
testing.  */

const elementChecker = (arr, value) => {
    if (arr.length === 0) {
        return "error - passed in array is empty";
    } else {
        return arr.includes(value);
    }
}
console.log(elementChecker(students, "Jane"));
console.log(elementChecker([], "Jane"));


/* 4. Create a function named checkAllStringsEnding that will accept a passed in array and
a character.The function will do the ff:
● if array is empty, return "error - array must NOT be empty"
● if at least one array element is NOT a string, return "error - all array elements must
be strings"
● if 2nd argument is NOT of data type string, return "error - 2nd argument must be of
data type string"
● if 2nd argument is more than 1 character long, return "error - 2nd argument must be
a single character"
● if every element in the array ends in the passed in character, return true.Otherwise
return false.
Use the students array and the character "e" as arguments when testing. */

const checkAllStringsEnding = (arr, value) => {
    if (arr.length === 0) {
        return "error - array must NOT be empty";
    } else if (!arr.every((elem) => typeof elem === "string")) {
        return "error - all array elements must be strings";
    } else if (typeof value !== "string") {
        return "error - 2nd argument must be of data type string";
    } else if (value.length !== 1) {
        return "error - 2nd argument must be a single character";
    } else {
        return arr.every((elem) => elem.endsWith(value));

    }
}

console.log(checkAllStringsEnding(students, "e"));
console.log(checkAllStringsEnding([], "e"));
console.log(checkAllStringsEnding([students, 02], "e"));
console.log(checkAllStringsEnding(students, "el"));
console.log(checkAllStringsEnding(students, 4));


/* 5. Create a function named stringLengthSorter that will take in an array of
strings as its argument and sort its elements in an ascending order based
on their lengths. If at least one element is not a string, return "error - all
array elements must be strings". Otherwise, return the sorted array. Use
the students array to test. */

const stringLengthSorter = (arr) => {

    if (!arr.every((el) => typeof el === 'string')) {
        return 'error - all array elements must be strings';
    }

    // Sort the array in ascending order based on the length of the strings
    return arr.sort((a, b) => a.length - b.length);
}

console.log(stringLengthSorter(students))
console.log(stringLengthSorter(["Jane", 037, "John", 039]))


/* 6. Create a function named startsWithCounter that will take in an array of strings and a
single character. The function will do the ff:
● if array is empty, return "error - array must NOT be empty"
● if at least one array element is NOT a string, return "error - all array elements must
be strings"
● if 2nd argument is NOT of data type string, return "error - 2nd argument must be of
data type string"
● if 2nd argument is more than 1 character long, return "error - 2nd argument must be
a single character"
● return the number of elements in the array that start with the character argument,
must be case-insensitive
Use the students array and the character "J" as arguments when testing. */

const startsWithCounter = (arr, char) => {
    // Check if array is empty
    if (arr.length === 0) {
        return "error - array must NOT be empty";
    }

    // Check if all array elements are strings
    if (!arr.every((el) => typeof el === "string")) {
        return "error - all array elements must be strings";
    }

    // Check if char is a string and has length 1
    if (typeof char !== "string") {
        return "error - 2nd argument must be of data type string";
    } else if (char.length !== 1) {
        return "error - 2nd argument must be a single character";
    }

    // Count number of elements that start with char (case-insensitive)
    const charLowerCase = char.toLowerCase();
    let count = 0;
    for (let i = 0; i < arr.length; i++) {
        const str = arr[i];
        if (str.charAt(0).toLowerCase() === charLowerCase) {
            count++;
        }
    }

    return count;
};

console.log(startsWithCounter(students, "j"));



/* 7. Create a function named likeFinder that will take in an array of strings and a string to
be searched for. The function will do the ff:
● if array is empty, return "error - array must NOT be empty"
● if at least one array element is NOT a string, return "error - all array elements must
be strings"
● if 2nd argument is NOT of data type string, return "error - 2nd argument must be of
data type string"
● return a new array containing all elements of the array argument that have the string
argument in it, must be case-insensitive
Use the students array and the character "jo" as arguments when testing.
 */

function likeFinder(arr, value) {
    // Check if array is empty
    if (arr.length === 0) {
        return "error - array must NOT be empty";
    }

    // Check if all elements are strings
    for (let i = 0; i < arr.length; i++) {
        if (typeof arr[i] !== "string") {
            return "error - all array elements must be strings";
        }
    }

    // Check if search string is a string
    if (typeof value !== "string") {
        return "error - 2nd argument must be of data type string";
    }

    // Create new array with matching elements
    let result = [];
    for (let i = 0; i < arr.length; i++) {
        if (arr[i].toLowerCase().includes(value.toLowerCase())) {
            result.push(arr[i]);
        }
    }

    return result;
}

console.log(likeFinder(students, "jo"));




/* 8. Create a function named randomPicker that will take in an array and
output any one of its elements at random when invoked. Pass in the
students array as an argument when testing. */

function randomPicker(arr) {
    if (arr.length === 0) {
        return "error - array must NOT be empty";
    }
    const randomIndex = Math.floor(Math.random() * arr.length);
    return arr[randomIndex];
}

console.log(randomPicker(students));

console.log(randomPicker(students));

console.log(randomPicker(students));

console.log(randomPicker(students));








